const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const hbs = require("hbs");
const passport = require("passport");
const session = require("express-session");

const apiRouter = require("./routes/api");
const apiAuthRouter = require("./routes/api/auth");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const dashboardRouter = require("./routes/dashboard");
const filmsRouter = require("./routes/films");
const actorsRouter = require("./routes/actors");
const aboutRouter = require("./routes/about");
const developerRouter = require("./routes/developer");

require("./config/api");
require("./config/db");
require("./config/fs");
require("./config/passport");

require("./models/user");
require("./models/film");
require("./models/actor");

const app = express();

// view engine setup
hbs.registerPartials(__dirname + "/views/partials");

app.engine("hbs", hbs.__express);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

// middlewares
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(
	session({
		secret: "ya lublu kpi(net)",
		resave: true,
		saveUninitialized: false,
		name: "awecin-session",
		cookie: {
			maxAge: 3600000 * 24 * 3 // 3 days (3600000 is hour)
		}
	})
);
app.use(passport.initialize());
app.use(passport.session());

// set locals user for use in hbs template
// set pageincludes to exclude extra browser js
app.use(function (req, res, next) {
	res.locals.pageincludes = [""];

	res.locals.user = req.user;
	if (!req.user) res.locals.pageincludes.push("auth"); //enable modal auth module
	next();
});

// Custom flash middleware -- from Ethan Brown's book, 'Web Development with Node & Express'
// https://gist.github.com/brianmacarthur/a4e3e0093d368aa8e423
app.use(function (req, res, next) {
	res.locals.flashMessage = req.session.flashMessage;
	delete req.session.flashMessage;
	next();
});

// routes
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/films", filmsRouter);
app.use("/actors", actorsRouter);
app.use("/dashboard", dashboardRouter);
app.use("/about", aboutRouter);
app.use("/developer", developerRouter);
app.use("/api/v1", apiRouter);
app.use("/api/v1/auth", apiAuthRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	const status = err.status || 500;
	res.status(status);

	res.locals.is4xx = status >= 400 && status < 500 ? true : false;

	res.render("error", {
		title: "Awecin - Error",
		bodyclass: "error-page"
	});
});

module.exports = app;
