const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");

cloudinary.config({
	cloud_name: process.env.CLOUD_NAME || "awecin",
	api_key: process.env.API_KEY || "775833174654551",
	api_secret: process.env.API_SECRET || "QO-bwstTgl-UxIyhDW8G69GE3Wc"
});

const storage = cloudinaryStorage({
	cloudinary: cloudinary,
	folder: "test1",
	allowedFormats: ["jpg", "png"],
});

const parser = multer({
	storage: storage,
	limits :{fileSize :52428800}
});

module.exports = parser;