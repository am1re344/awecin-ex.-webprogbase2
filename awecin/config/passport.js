const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const BasicStrategy = require("passport-http").BasicStrategy;

const debug = require("debug")("awecin:passport");

const User = require("../models/user");

passport.serializeUser(function(user, done) {
	done(null, user._id);
});

passport.deserializeUser(function(id, done) {
	User.findOne({ _id: id })
		.exec()
		.then(docs => done(null, docs))
		.catch(err => {
			debug("[deserializeUser]" + err);
			done(err);
		});
});

passport.use(
	new LocalStrategy((username, password, done) => {
		User.findOne({ username: username })
			.exec()
			.then(docs => {
				if (docs && docs.validPassword(password)) done(null, docs);
				else done(null, false, { message: "Invalid username or password." });
			})
			.catch(err => {
				debug("[LocalStrategy]" + err);
				done(err);
			});
	})
);

passport.use(
	new BasicStrategy((username, password, done) => {
		const serveruser = process.env.SERVER_API_USER || "dI6pth6W1G5ikDAdICwR";
		const serverpass = process.env.SERVER_API_PASS || "tEWBZagYMeQacaTgxF0y";

		if(username == serveruser && password == serverpass) return done(null, {username: "server"});

		User.findOne({ username: username })
			.exec()
			.then(docs => {
				if (docs && docs.validPassword(password) && docs.role == 0) done(null, docs);
				else done(null, false);
			})
			.catch(err => {
				debug("[BasicStrategy]" + err);
				done(err);
			});
	})
);
