const mongoose = require("mongoose");
const debug = require("debug")("awecin:database");

const dburl = process.env.DB_URL || "mongodb://root:pGaZa65{Z~@ds223063.mlab.com:23063/awecin-test1";

mongoose.connect(
	dburl,
	{ useNewUrlParser: true }
);

const db = mongoose.connection;

db.on("error", (err) => {
	debug("" + err);
	console.error.bind(console, "connection error:");
	process.exit(1);
});
db.once("open", function () {
	debug("connected");
});

module.exports = db;