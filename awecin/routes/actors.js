const express = require("express");
const router = express.Router();

const FormData = require("form-data");
const multer = require("multer");

const auth = require("../helpers/auth");
const actorsHelper = require("../helpers/actorsPartials");
const api = require("../config/api");

router.get("/", async function (req, res, next) {
	const allowedKeys = actorsHelper.getAllowedKeysFilter(); //allowed keys for search

	const ajax = req.query.ajax == true ? true : false;
	delete req.query.ajax;

	req.query.page = parseInt(req.query.page) - 1;
	const page = Number.isInteger(req.query.page) && req.query.page > -1 ? req.query.page : 0;
	delete req.query.page;

	let queryForAPI = "?page=" + page;
	for (let key in req.query) {
		if (!allowedKeys.includes(key)) continue;

		if (key == "fullname") {
			req.query[key] = "/" + req.query[key] + "/i";
		}
		if (key == "born>" || key == "born<") {
			const date = new Date(req.query[key]);
			date.setMonth(1);
			date.setDate(1);
			req.query[key] = date.toISOString();
		}

		queryForAPI += "&" + key + "=" + req.query[key] + "";
	}

	if(req.query.films && req.query.films.length > 0){ // add film title to filters
		const slugs = req.query.films.split(",");
		if (Array.isArray(slugs)){
			for(let i = 0; i < slugs.length; i++){
				await api.get("/films/" + slugs[i]).then(resp => {
					if(resp.data.data && resp.data.data.title) {
						slugs[i] = {name : resp.data.data.title, slug : slugs[i]};
					}
				});
			}
			req.query.films = slugs;
		}
		else {
			await api.get("/films/" + slugs).then(resp => req.query.films = {name : resp.data.data.title, slug : req.query.films});
		}
	}

	api
		.get("/actors" + queryForAPI)
		.then(response => {
			const filterHtml = actorsHelper.filtersBlock(req.query).toString();

			const html = actorsHelper.actorsBlock(response.data.data, response.data.page + 1, response.data.count, response.data.limit, req.query);
			if(ajax) return res.send(html.toString());

			res.locals.pageincludes.push("nouislider", "selectize");
			res.render("actors", {
				title: "Awecin - Actors",
				bodyclass: "actors-page",
				actorsBlock: html,
				filtersBlock: filterHtml
			});
		})
		.catch(err => {
			if (err.response) {
				//
			} else if (err.request) {
				//
			} else {
				//
			}
			next(err);
		});
});

router.post("/", auth.isAdmin, multer().any(), async function (req, res, next) {  //async await for good read & chain promises
	let slug,
		imgurl,
		message = "";

	await api
		.post("/actors", req.body)
		.then(response => {
			message += "Actor '" + response.data.data.fullname + "' added to DB successfully";
			slug = response.data.data.slug;
		})
		.catch(err => {
			if (err.response) message += err.response.data.message;
			else next(err);
		});

	if (!req.files || !req.files[0]) message += " (without ava)";
	else if (slug) {
		const formData = new FormData();
		formData.append("image", req.files[0].buffer, {
			filename: req.files[0].originalname,
			knownLength: req.files[0].size,
			contentType: req.files[0].mimetype
		});

		const config = {
			headers: formData.getHeaders()
			// maxContentLength: 52428890 //50 mb
		};

		await api
			.post("/storage", formData, config)
			.then(response => {
				if (response.data.data) imgurl = response.data.data.url;
				else message += "(problems with image upload)";
			})
			.catch(err => {
				message += "(failed to upload image)";
				if (!err.response) next(err);
			});

		if (imgurl) api.put("/actors/" + slug, { imgurl: imgurl }).catch(err => next(err)); //attach img link to film
	}

	req.session.flashMessage = {
		type: slug ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.get("/:slug", function (req, res, next) {
	const slug = req.params.slug;

	api
		.get("/actors/" + slug)
		.then(async response => {
			if (response.data.data.born) {
				const date = new Date(response.data.data.born);
				response.data.data.born = date.toLocaleDateString("en-US", { year: "numeric", month: "long", day: "numeric" });
			}

			let films = [];
			if(response.data.data.films){
				const slugs = response.data.data.films;
				if(slugs && Array.isArray(slugs)){
					for(let i = 0; i < slugs.length; i++){
						await api.get("/films/" + slugs[i]).then(resp => {
							if(resp.data.data && resp.data.data.releaseDate){
								const date = new Date(resp.data.data.releaseDate);
								resp.data.data.year = date.getFullYear();
							}
							films.push(resp.data.data);
						});
					}
				}
			}

			res.render("actor", {
				bodyclass: "actor-page",
				actor: response.data.data,
				films
			});
		})
		.catch(err => {
			if (err.response) {
				//
			} else if (err.request) {
				//
			} else {
				//
			}
			next(err);
		});
});

router.post("/delete", auth.isAdmin, async function (req, res, next) {
	const slug = req.body.actor;
	let message = "",
		type = 1;

	await api
		.delete("/actors/" + slug)
		.then(response => {
			message = "Actor deleted successfully";
		})
		.catch(err => {
			message = "Please, try later (" + err.toString() + ")";
			type = 0;
		});

	req.session.flashMessage = {
		type: type ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.post("/update", auth.isAdmin, multer().any(), async function (req, res, next) {  //async await for good read & chain promises
	const slug = req.body.actor;

	let imgurl,
		message = "";

	await api
		.put("/actors/" + slug, req.body)
		.then(response => {
			message += "Actor '" + response.data.data.fullname + "' updated successfully";
		})
		.catch(err => {
			if (err.response) message += err.response.data.message;
			else next(err);
		});

	if (!req.files || !req.files[0]) message += " (without ava)";
	else if (slug) {
		const formData = new FormData();
		formData.append("image", req.files[0].buffer, {
			filename: req.files[0].originalname,
			knownLength: req.files[0].size,
			contentType: req.files[0].mimetype
		});

		const config = {
			headers: formData.getHeaders()
			// maxContentLength: 52428890 //50 mb
		};

		await api
			.post("/storage", formData, config)
			.then(response => {
				if (response.data.data) imgurl = response.data.data.url;
				else message += "(problems with image upload)";
			})
			.catch(err => {
				message += "(failed to upload image)";
				if (!err.response) next(err);
			});

		if (imgurl) api.put("/actors/" + slug, { imgurl: imgurl }).catch(err => next(err)); //attach img link to film
	}

	req.session.flashMessage = {
		type: slug ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

module.exports = router;