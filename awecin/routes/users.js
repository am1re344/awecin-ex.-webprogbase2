const express = require("express");
const router = express.Router();

const FormData = require("form-data");
const multer = require("multer");

const auth = require("../helpers/auth");
const api = require("../config/api");

router.post("/update", auth.isLoggedIn, multer().any(), async function (req, res, next) {  //async await for good read & chain promises
	const username = (auth.isAdmin && req.body.user && req.body.user.length > 0) ? req.body.user : req.user.username;

	let imgurl,
		message = "";

	await api
		.put("/users/" + username, req.body)
		.then(response => {
			message += "User '" + response.data.data.username + "' updated successfully";
		})
		.catch(err => {
			if (err.response) message += err.response.data.message;
			else next(err);
		});

	if (!req.files || !req.files[0]) message += " (without ava)";
	else if (username) {
		const formData = new FormData();
		formData.append("image", req.files[0].buffer, {
			filename: req.files[0].originalname,
			knownLength: req.files[0].size,
			contentType: req.files[0].mimetype
		});

		const config = {
			headers: formData.getHeaders()
			// maxContentLength: 52428890 //50 mb
		};

		await api
			.post("/storage", formData, config)
			.then(response => {
				if (response.data.data) imgurl = response.data.data.url;
				else message += "(problems with image upload)";
			})
			.catch(err => {
				message += "(failed to upload image)";
				if (!err.response) next(err);
			});

		if (imgurl) api.put("/users/" + username, { imgurl: imgurl }).catch(err => next(err)); //attach img link to film
	}

	req.session.flashMessage = {
		type: username ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.post("/delete", auth.isAdmin, async function (req, res, next) {
	const username = req.body.user;
	let message = "",
		type = 1;

	await api
		.delete("/users/" + username)
		.then(response => {
			message = "User deleted successfully";
		})
		.catch(err => {
			message = "Please, try later (" + err.toString() + ")";
			type = 0;
		});

	req.session.flashMessage = {
		type: type ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

module.exports = router;