const express = require("express");
const router = express.Router();

const FormData = require("form-data");
const multer = require("multer");

const auth = require("../helpers/auth");
const filmsHelper = require("../helpers/filmsPartials");
const api = require("../config/api");

router.get("/", function (req, res, next) {
	const allowedKeys = filmsHelper.getAllowedKeysFilter(); //allowed keys for search

	const ajax = req.query.ajax == true ? true : false;
	delete req.query.ajax;

	req.query.page = parseInt(req.query.page) - 1;
	const page = Number.isInteger(req.query.page) && req.query.page > -1 ? req.query.page : 0;
	delete req.query.page;

	let queryForAPI = "?page=" + page;
	for (let key in req.query) {
		if (!allowedKeys.includes(key)) continue;

		if (key == "genre") {
			key = "genre.slug";
			req.query[key] = req.query["genre"].toString().toLowerCase();
			delete req.query["genre"];
		}
		if (key == "title") {
			req.query[key] = "/" + req.query[key] + "/i";
		}
		if (key == "releaseDate>" || key == "releaseDate<") {
			const date = new Date(req.query[key]);
			date.setMonth(1);
			date.setDate(1);
			req.query[key] = date.toISOString();
		}

		queryForAPI += "&" + key + "=" + req.query[key] + "";
	}

	api
		.get("/films" + queryForAPI)
		.then(response => {
			return Promise.all([response, api.get("/genres")]);
		})
		.then(([films, genres]) => {
			const filterHtml = filmsHelper.filtersBlock(genres.data.data, req.query).toString();

			const html = filmsHelper.filmsBlock(films.data.data, films.data.page + 1, films.data.count, films.data.limit, req.query);
			if (ajax) return res.send(html.toString());

			res.locals.pageincludes.push("nouislider");
			res.render("films", {
				title: "Awecin - Films",
				bodyclass: "films-page",
				filmsBlock: html,
				filtersBlock: filterHtml
			});
		})
		.catch(err => {
			if (err.response) {
				//
			} else if (err.request) {
				//
			} else {
				//
			}
			next(err);
		});
});

router.post("/delete", auth.isAdmin, async function (req, res, next) {
	const slug = req.body.films;
	let message = "",
		type = 1;

	await api
		.delete("/films/" + slug)
		.then(response => {
			message = "Film deleted successfully";
		})
		.catch(err => {
			message = "Please, try later (" + err.toString() + ")";
			type = 0;
		});

	req.session.flashMessage = {
		type: type ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.post("/update", auth.isAdmin, multer().any(), async function (req, res, next) {
	const slug = req.body.films;

	let imgurl,
		message = "";

	await api
		.put("/films/" + slug, req.body)
		.then(response => {
			message += "Film '" + response.data.data.title + "' updated successfully";
		})
		.catch(err => {
			if (err.response) message += err.response.data.message;
			else next(err);
		});

	if (!req.files || !req.files[0]) message += " (without poster)";
	else if (slug) {
		const formData = new FormData();
		formData.append("image", req.files[0].buffer, {
			filename: req.files[0].originalname,
			knownLength: req.files[0].size,
			contentType: req.files[0].mimetype
		});

		const config = {
			headers: formData.getHeaders()
			// maxContentLength: 52428890 //50 mb
		};

		await api
			.post("/storage", formData, config)
			.then(response => {
				if (response.data.data) imgurl = response.data.data.url;
				else message += "(problems with image upload)";
			})
			.catch(err => {
				message += "(failed to upload image)";
				if (!err.response) next(err);
			});

		if (imgurl) api.put("/films/" + slug, { imgurl: imgurl }).catch(err => next(err)); //attach img link to film
	}

	req.session.flashMessage = {
		type: slug ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.post("/", auth.isAdmin, multer().any(), async function (req, res, next) {  //async await for good read & chain promises
	let slug,
		imgurl,
		message = "";

	await api
		.post("/films", req.body)
		.then(response => {
			message += "Film '" + response.data.data.title + "' added to DB successfully";
			slug = response.data.data.slug;
		})
		.catch(err => {
			if (err.response) message += err.response.data.message;
			else next(err);
		});

	if (!req.files || !req.files[0]) message += " (without poster)";
	else if (slug) {
		const formData = new FormData();
		formData.append("image", req.files[0].buffer, {
			filename: req.files[0].originalname,
			knownLength: req.files[0].size,
			contentType: req.files[0].mimetype
		});

		const config = {
			headers: formData.getHeaders()
			// maxContentLength: 52428890 //50 mb
		};

		await api
			.post("/storage", formData, config)
			.then(response => {
				if (response.data.data) imgurl = response.data.data.url;
				else message += "(problems with image upload)";
			})
			.catch(err => {
				message += "(failed to upload image)";
				if (!err.response) next(err);
			});

		if (imgurl) api.put("/films/" + slug, { imgurl: imgurl }).catch(err => next(err)); //attach img link to film
	}

	req.session.flashMessage = {
		type: slug ? "success" : "danger",
		message: message,
		hash: req.body.currentTab
	};

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

router.get("/:slug", function (req, res, next) {
	const slug = req.params.slug;

	api
		.get("/films/" + slug)
		.then(response => Promise.all([
			response,
			api.get("/actors?limit=4&films=" + slug)
		]))
		.then(([filmsres, actorsres]) => {
			if (filmsres.data && filmsres.data.data) {
				if (filmsres.data.data.releaseDate) {
					const date = new Date(filmsres.data.data.releaseDate);
					filmsres.data.data.releaseDate = date.toLocaleDateString("en-US", { year: "numeric", month: "long", day: "numeric" });
				}
				if (filmsres.data.data.rating) {
					const intRating = Math.floor(filmsres.data.data.rating / 2);
					let objRating = { filled: [], empty: ["_", "_", "_", "_", "_"] };
					for (let i = 0; i < intRating; i++) {
						objRating.filled.push("_");
						delete objRating.empty[i];
					}
					filmsres.data.data.objRating = objRating;
				}
				if (req.user && req.user.watchlist) {
					req.user.watchlist.forEach(currentslug => {
						if (slug == currentslug) filmsres.data.data.inwatchlist = true;
					});
				}
			}

			res.render("film", {
				bodyclass: "film-page",
				film: filmsres.data.data,
				actors: actorsres.data.data
			});
		})
		.catch(err => {
			if (err.response) {
				//
			} else if (err.request) {
				//
			} else {
				//
			}
			next(err);
		});
});

router.post("/:slug/watchlist", auth.isLoggedIn, async function (req, res, next) {
	const slug = req.params.slug;

	await api.post("/users/" + req.user.username + "/watchlist", { film: slug })
		.then(response => {
			//
		})
		.catch(err => {
			// if(err.response && err.response.status == 400){
			// 	console.log(err.response.data.message);
			// }
			// else next(err);
			next(err);
		});

	const backUrl = req.header("Referer").split("?")[0] + "#" + (req.body.currentTab || "");
	return res.redirect(backUrl);
});

module.exports = router;
