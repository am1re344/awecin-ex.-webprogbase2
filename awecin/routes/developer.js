const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	res.render("developer",{
		bodyclass: "dev-page",
		title: "Awecin - Developer API Docs"
	});
});

module.exports = router;