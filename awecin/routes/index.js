const express = require("express");
const router = express.Router();

router.get("/", function(req, res) {
	res.render("index", {
		title: "Awecin - Main Page",
		bodyclass: "index-page"
	});
});

module.exports = router;
