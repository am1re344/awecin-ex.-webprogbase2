const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	res.render("about",{
		bodyclass: "about-page",
		title: "Awecin - About"
	});
});

module.exports = router;