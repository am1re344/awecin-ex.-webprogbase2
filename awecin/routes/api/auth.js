const express = require("express");
const router = express.Router();
const passport = require("passport");

const createError = require("http-errors");
const debug = require("debug")("awecin:auth");

const api = require("../../config/api");

router.post("/login", (req, res, next) => {
	const isajax = req.query.ajax == 1 ? true : false;
	passport.authenticate("local", function(err, user, info) {
		if (err) {
			debug("" + err);
			return isajax ? res.send(err) : next(err);
		}

		if (!user) {
			const err = createError(400, info.message);
			return isajax ? res.send(err) : next(err);
		}

		req.login(user, function(err) {
			if (err) {
				debug("" + err);
				return isajax ? res.send(err) : next(err);
			}
			isajax ? res.send(user) : res.redirect("/");
		});
	})(req, res, next);
});

router.post("/logout", (req, res) => {
	req.logout();
	const backUrl = req.header("Referer");
	return res.redirect(backUrl);
});

router.post("/register", (req, res, next) => {
	const isajax = req.query.ajax == 1 ? true : false;

	const username = req.body.username,
		password = req.body.password,
		password2 = req.body.password_confirm;

	if (!username || !password || password != password2) {
		const err = createError(400, "Passwords does not match");
		return isajax ? res.send(err) : next(err);
	}

	api
		.post("/users", {
			username: username,
			password: password
		})
		.then(response => isajax ? res.send(response.data.data) : res.redirect("/?" + response.data.data.username) )
		.catch(err => {
			debug("" + err);
			if (err.response) {
				const newerr = createError(400, err.response.data.message);
				return isajax ? res.send(newerr) : next(newerr);
			} else return isajax ? res.send(err) : next(err);
		});
});

module.exports = router;
