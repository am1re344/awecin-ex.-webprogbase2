const express = require("express");
const router = express.Router();
const passport = require("passport");
const aqp = require("api-query-params");

const debug = require("debug")("awecin:api");

const User = require("../../models/user");
const Film = require("../../models/film");
const Actor = require("../../models/actor");

const storage = require("../../config/fs");

const makeSlug = require("../../helpers/makeslug");

router.get("/genres", function(req, res){
	Film.find({}).distinct("genre").then(response => {
		res.status(200).json({data : response});
	})
		.catch(err => {
			debug("" + err);
			res.status(500).json({message: err.toString()});
		});
});

router.get("/users", passport.authenticate("basic", { session: false }), function(req, res) {
	const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 6;
	delete req.query.limit;

	req.query.page = parseInt(req.query.page);
	const page = Number.isInteger(req.query.page) && req.query.page > -1 ? req.query.page : 0;
	delete req.query.page;

	req.query.skip = limit * page;

	const { filter, skip, sort, projection } = aqp(req.query);

	User.paginate(filter, {
		page: page,
		limit: limit,
		offset: skip,
		sort: sort,
		select: projection
	})
		.then(result => {
			res.status(200).json({
				page,
				count: result.total,
				limit: result.limit,
				data: result.docs
			});
		})
		.catch(err => {
			debug("" + err);
			res.status(500).json({ message: err.toString() });
		});
}
);

router.post("/users", passport.authenticate("basic", { session: false }), function(req, res) {
	const password = req.body.password;
	if (!password || password.length < 4 || password.length > 16)
		return res.status(400).json({ message: "Invalid password" });

	const user = new User({
		username: req.body.username,
		role: 1
	});

	user.setPassword(req.body.password);

	user
		.save()
		.then(result => res.status(200).json({ data: result }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.get("/username/:username", function(req, res) {
	const username = req.params.username;

	User.findOne({ username: username })
		.exec()
		.then(docs => {
			const exists = docs && docs.username ? true : false;
			res.status(200).json({ user: exists });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.get("/users/:username", passport.authenticate("basic", { session: false }), function(req, res) {
	const username = req.params.username;

	User.findOne({ username: username })
		.exec()
		.then(docs => {
			res.status(200).json({ data: docs });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.get("/users/:username/watchlist", passport.authenticate("basic", { session: false }), function(req, res) {
	const username = req.params.username;

	User.findOne({ username: username })
		.exec()
		.then(docs => {
			res.status(200).json({ data: docs.watchlist });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});	
});

router.post("/users/:username/watchlist", passport.authenticate("basic", { session: false }), function(req, res) {
	const username = req.params.username;
	const filmslug = req.body.film;

	User.findOne({ username: username })
		.exec()
		.then(user => {
			if(filmslug) return Promise.all([user, Film.findOne({slug : filmslug}).exec()]);
			else return res.status(400).json({ message: "Bad slug" });
		})
		.then(([user, film]) => {
			if(!film) return res.status(400).json({ message: "Bad slug" });
			
			const index = user.watchlist.indexOf(filmslug);
			if(index === -1) user.watchlist.push(film.slug);
			else user.watchlist.splice(index, 1);
			
			return user.save();
		})
		.then(docs => res.status(200).json({ data: docs }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.get("/films", function(req, res) {
	const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 6;
	delete req.query.limit;

	req.query.page = parseInt(req.query.page);
	const page = Number.isInteger(req.query.page) && req.query.page > -1 ? req.query.page : 0;
	delete req.query.page;

	req.query.skip = limit * page;

	const { filter, skip, sort, projection } = aqp(req.query);

	Film.paginate(filter, {
		page: page,
		limit: limit,
		offset: skip,
		sort: sort,
		select: projection
	})
		.then(result => {
			res.status(200).json({
				page,
				count: result.total,
				limit: result.limit,
				data: result.docs
			});
		})
		.catch(err => {
			debug("" + err);
			res.status(500).json({ message: err.toString() });
		});
});

router.post("/films", passport.authenticate("basic", { session: false }), function(req, res) {
	const title = req.body.title;
	delete req.body.title;

	const shortdesc = req.body.shortdesc;
	delete req.body.shortdesc;

	const genre = req.body.genre;
	delete req.body.genre;
	
	let genres = [];
	if (genre && Array.isArray(genre)) {
		genre.forEach(name => {
			const slug = makeSlug(name);
			genres.push({ name: name, slug: slug });
		});
	}
	else if(genre) {
		const slug = makeSlug(genre);
		genres.push({ name: genre, slug: slug });
	}

	const film = new Film({
		title: title,
		shortdesc: shortdesc,
		genre: genres
	});

	Object.keys(req.body).forEach(function(key) {
		if (key == "rating") req.body[key] = req.body[key] ? parseFloat(req.body[key]) : undefined;
		if (key == "genre") return;
		film[key] = req.body[key] ? req.body[key] : undefined;
	});

	film.setSlug(title);

	film
		.save()
		.then(docs => res.status(200).json({ data: docs }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
}
);

router.get("/films/:slug", function(req, res) {
	const slug = req.params.slug;

	Film.findOne({ slug: slug })
		.exec()
		.then(docs => res.status(200).json({ data: docs }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.delete("/films/:slug", passport.authenticate("basic", { session: false }), (req, res) => {
	const slug = req.params.slug;

	Film.deleteOne({ slug: slug })
		.exec()
		.then(() => {
			res.status(200).json({ message: "Success" });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.put("/films/:slug", passport.authenticate("basic", { session: false }), (req, res) => {
	const slug = req.params.slug;

	Film.findOne({ slug: slug })
		.exec()
		.then(docs => {
			if (!docs) return res.status(404).json({ data: docs });

			const genre = req.body.genre;
			delete req.body.genre;
			let genres = [];
			if (genre && Array.isArray(genre)) {
				genre.forEach(name => {
					const slug = makeSlug(name);
					genres.push({ name: name, slug: slug });
				});
			}
			else if(genre) {
				const slug = makeSlug(genre);
				genres.push({ name: genre, slug: slug });
			}

			docs.genre = genres;

			Object.keys(req.body).forEach(function(key) {
				if(req.body[key].length <= 0) return;
				
				if (key == "rating") req.body[key] = parseFloat(req.body[key]);
				if (key == "genre") return;
				docs[key] = req.body[key];
			});

			return docs.save();
		})
		.then(docs => {
			res.status(200).json({ data: docs });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

// <input type='file' name='image' /> img only
router.post( "/storage", passport.authenticate("basic", { session: false }), storage.single("image"), function(req, res) {
	debug("file upploaded (" + req.file.public_id + ")");
	res.status(200).json({ data: req.file });
});

router.get("/actors", function(req, res) {
	const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 6;
	delete req.query.limit;

	req.query.page = parseInt(req.query.page);
	const page = Number.isInteger(req.query.page) && req.query.page > -1 ? req.query.page : 0;
	delete req.query.page;

	req.query.skip = limit * page;

	const { filter, skip, sort, projection } = aqp(req.query);

	Actor.paginate(filter, {
		page: page,
		limit: limit,
		offset: skip,
		sort: sort,
		select: projection
	})
		.then(result => {
			res.status(200).json({
				page,
				count: result.total,
				limit: result.limit,
				data: result.docs
			});
		})
		.catch(err => {
			debug("" + err);
			res.status(500).json({ message: err.toString() });
		});
});

router.post("/actors", passport.authenticate("basic", { session: false }), function(req, res) {
	const fullname = req.body.fullname;
	delete req.body.fullname;

	const shortbio = req.body.shortbio;
	delete req.body.shortbio;

	let films;
	if(!req.body.films) films = undefined;
	else {
		if(req.body.films.indexOf(",") == -1) {
			films = req.body.films;
		}
		else films = req.body.films.split(",");
	}
	delete req.body.films;

	const actor = new Actor({
		fullname: fullname,
		shortbio: shortbio,
		films: films
	});

	Object.keys(req.body).forEach(function(key) {
		actor[key] = req.body[key] ? req.body[key] : undefined;
	});

	actor.setSlug(fullname);

	actor
		.save()
		.then(docs => res.status(200).json({ data: docs }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.get("/actors/:slug", function(req, res) {
	const slug = req.params.slug;

	Actor.findOne({ slug: slug })
		.exec()
		.then(docs => res.status(200).json({ data: docs }))
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.delete("/actors/:slug", passport.authenticate("basic", { session: false }), (req, res) => {
	const slug = req.params.slug;

	Actor.deleteOne({ slug: slug })
		.exec()
		.then(() => {
			res.status(200).json({ message: "Success" });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.put("/actors/:slug", passport.authenticate("basic", { session: false }), (req, res) => {
	const slug = req.params.slug;

	Actor.findOne({ slug: slug })
		.exec()
		.then(docs => {
			if (!docs) return res.status(404).json({ data: docs });

			const films = req.body.films ? req.body.films.split(",") : undefined;
			docs.films = films;

			Object.keys(req.body).forEach(function(key) {
				if(key == "films") return;
				docs[key] = req.body[key];
			});

			return docs.save();
		})
		.then(docs => {
			res.status(200).json({ data: docs });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.put("/users/:username", passport.authenticate("basic", { session: false }), (req, res) => {
	const username = req.params.username;

	User.findOne({ username: username })
		.exec()
		.then(docs => {
			if (!docs) return res.status(404).json({ data: docs });

			Object.keys(req.body).forEach(function(key) {
				if(req.body[key].length <= 0) return;
				docs[key] = req.body[key];
			});

			return docs.save();
		})
		.then(docs => {
			res.status(200).json({ data: docs });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

router.delete("/users/:username", passport.authenticate("basic", { session: false }), (req, res) => {
	const username = req.params.username;

	User.deleteOne({ username: username })
		.exec()
		.then(() => {
			res.status(200).json({ message: "Success" });
		})
		.catch(err => {
			debug("" + err);
			res.status(400).json({ message: err.toString() });
		});
});

module.exports = router;
