const express = require("express");

const router = express.Router();

const auth = require("../helpers/auth");
const api = require("../config/api");

router.get("/", auth.isLoggedIn, async (req, res) => {
	let watchlist = [];
	if(req.user && req.user.watchlist){
		const slugs = req.user.watchlist;
		if(slugs && Array.isArray(slugs)){
			for(let i = 0; i < slugs.length; i++){
				await api.get("/films/" + slugs[i]).then(resp => {
					if(resp.data.data && resp.data.data.releaseDate){
						const date = new Date(resp.data.data.releaseDate);
						resp.data.data.year = date.getFullYear();
					}

					if(resp.data.data) watchlist.push(resp.data.data);
				});
			}
		}
	}

	res.locals.pageincludes.push("tagsinput", "datepicker", "selectize");
	res.render("dashboard", {
		title: "Awecin - Dashboard",
		bodyclass: "dashboard-page",
		isadmin: req.user.role == 0 ? true : false,
		watchlist
	});
});

module.exports = router;
