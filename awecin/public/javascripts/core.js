/*global jQuery*/
(function ($) {
	"use strict";
	
	function AwecinClass() {
		const self = this;

		self.BASE_API = "/api/v1";
		self.STYLES_URL = "/stylesheets/libs/";
		self.SCRIPTS_URL = "/javascripts/libs/";

		self.$body = $("body");
		self.$pageIncludes = $("#page-includes");

		// auth
		self.$signinform = $("#signinmodal form#signin");
		self.$signupform = $("#signupmodal form#signup");

		self.authLoaderTime = 500;
		self.authPageRedirectTime = 1000;

		//films
		self.$searchWrap = $(".search-wrap");

		self.playVideoBtn = "a.play-video.btn";
		self.$playVideoModal = $("#playvideomodal");
		self.$playVideo = self.$playVideoModal.find("iframe#video");

		// scripts
		self.scripts = [
			//
		];
		// dashboard dependencies
		self.tagsinputStyleUrl = self.STYLES_URL + "bootstrap-tagsinput/bootstrap-tagsinput.css";
		self.tagsinputScriptUrl = self.SCRIPTS_URL + "bootstrap-tagsinput/bootstrap-tagsinput.min.js";

		self.datePickerStyleUrl = self.STYLES_URL + "datepicker/datepicker.min.css";
		self.datePickerScriptUrl = self.SCRIPTS_URL + "datepicker/datepicker.min.js";
		
		// films filters dependencies
		self.nouislierStyleUrl = self.STYLES_URL + "nouislider/nouislider.min.css";
		self.nouislierScriptUrl = self.SCRIPTS_URL + "nouislider/nouislider.min.js";

		self.selectizeSript = self.SCRIPTS_URL + "selectize/selectize.min.js";
		// self.selectizeStyle = self.STYLES_URL + "selectize/selectize.bootstrap3.css";
		self.selectizeStyle = self.STYLES_URL + "selectize/selectize.css";

		
		self.init();
	}

	AwecinClass.prototype = {
		init: async function () {
			const self = this;

			self.lowInit();
			
			let scripts = self.scripts;
			let styles = [];
			
			if (self.$pageIncludes.hasClass("selectize")) {
				scripts.push(self.selectizeSript);
				styles.push(self.selectizeStyle);	
			}

			if (self.$pageIncludes.hasClass("nouislider")) {
				scripts.push(self.nouislierScriptUrl);
				styles.push(self.nouislierStyleUrl);				
			}

			if (self.$pageIncludes.hasClass("tagsinput")) {
				scripts.push(self.tagsinputScriptUrl);
				styles.push(self.tagsinputStyleUrl);
			}
			
			if (self.$pageIncludes.hasClass("datepicker")) {
				scripts.push(self.datePickerScriptUrl);
				styles.push(self.datePickerStyleUrl);
			}
			
			for(let i = 0; i < styles.length; i++){
				$.loadScript(styles[i], "text")
					.then(css => $("head").append("<style>" + css + "</style>"));
			}

			for(let i = 0; i < scripts.length; i++){
				await $.loadScript(scripts[i]);
			}

			self.initCore();
		},
		lowInit: function(){
			const self = this;

			if (self.$body.hasClass("dashboard-page")) {
				self.dashBoardSetup();
			}

			if (self.$body.hasClass("films-page")) {
				self.filmsPage();
			}
			
			if (self.$body.hasClass("film-page")) {
				self.filmPage();
			}

			if (self.$body.hasClass("actors-page")) {
				self.actorsPage();
			}

			if (self.$pageIncludes.hasClass("auth")) {
				self.signInAjax();
				self.signUpValidation();
			}
		},
		initCore: function(){
			const self = this;
			
			if (self.$body.hasClass("dashboard-page")) {
				self.syncDashBoardSetup();
			}

			if (self.$body.hasClass("films-page")) {
				self.syncFilmsPage();
			}

			if (self.$body.hasClass("actors-page")) {
				self.syncActorsPage();
			}
		},
		actorsPage: function(){
			const self = this;

			self.$searchWrap.on("click", ".pagination .page-item .page-link", function (e) {
				e.preventDefault();
				const url = $(this).attr("href");

				$.ajax({
					method: "GET",
					url: url + "&ajax=1",
					beforeSend: () => {
						self.$searchWrap.addClass("loading");
					},
					complete: () => {
						self.$searchWrap.removeClass("loading");
					},
					error: () => {
						self.$searchWrap.addClass("error");
					}
				}).done(function (response) {
					self.$searchWrap.html(response);
					if (history.pushState) window.history.pushState(null, null, url);
					else window.location.hash = url;
				});
			});	
		},
		syncActorsPage: function(){
			const self = this;

			$(".filters .dropdown-menu").on("click.bs.dropdown", function (e) {
				e.stopPropagation();
				e.preventDefault();
			});

			const filmsSelect = $("#films-select");
			const selectControl = filmsSelect.selectize({
				valueField: "title",
				labelField: "title",
				searchField: "title",
				options: [],
				create: false,
				onChange: () => updateField(),
				render: {
					item: (item, escape) => {
						if(item.title && (item.slug == "undefined" || !item.slug)){
							item.slug = item.title.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
						}

						return "<div data-slug='"+ escape(item.slug) + "'>" + escape(item.title) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.title) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/films?title=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			})[0].selectize;

			updateField();
			function updateField(){
				let value = [];
				const items = selectControl.getValue();
				if(Array.isArray(items)) {
					items.forEach(item => {
						value.push( selectControl.getItem(item).data("slug") );
					});
				}
				else {
					value.push( selectControl.getItem(items).data("slug") );
				}

				$("input[name='films']").val(value);
			}
			// genre
			$("#filter-name input#name").on("change", function(){
				$("input[name='fullname']").val($(this).val());
			});
			// year
			const yearSlider = document.getElementById("filter-year-slider");
			const dataStartYear = $(yearSlider).data("start");
			const startYear = Array.isArray(dataStartYear) ? dataStartYear : [1915, 2019];
			// eslint-disable-next-line no-undef
			noUiSlider.create(yearSlider, {
				start: startYear,
				step: 1,
				connect: true,
				range: {
					"min": 1915,
					"max": 2019
				},
				format: {
					to: function (value) {
						return value;
					},
					from: function (value) {
						return value;
					}
				}
			});
			yearSlider.noUiSlider.on("update", function(values, handle) {
				const labels = $("#filter-year .filter-btn .filter-range");
				const start = labels.find("#filter-range-start");
				const end = labels.find("#filter-range-end");

				if(!handle) {
					start.html(values[0]);
					$("input[name='born>']").val(values[0]);
				}
				else {
					end.html(values[1]);
					$("input[name='born<']").val(values[1]);
				}
			});
		},
		filmPage: function(){
			const self = this;
			const $button = $(".shortdetails .play-btn");
			
			$button.on("click", function (e) {
				let watch = $(this).data("watch");

				if (watch.length > 0) e.preventDefault();
				else return; //open default link otherwise

				self.$playVideo.attr("src", "https://www.youtube.com/embed/" + watch + "?rel=0&showinfo=0&modestbranding=1&autoplay=1");
				self.$playVideoModal.modal("show");
			});

			self.$playVideoModal.on("hide.bs.modal", function () {
				self.$playVideo.attr("src", "");
			});
		},
		filmsPage: function() {
			const self = this;

			self.$searchWrap.on("click", ".pagination .page-item .page-link", function (e) {
				e.preventDefault();
				const url = $(this).attr("href");

				$.ajax({
					method: "GET",
					url: url + "&ajax=1",
					beforeSend: () => {
						self.$searchWrap.addClass("loading");
					},
					complete: () => {
						self.$searchWrap.removeClass("loading");
					},
					error: () => {
						self.$searchWrap.addClass("error");
					}
				}).done(function (response) {
					self.$searchWrap.html(response);
					if (history.pushState) window.history.pushState(null, null, url);
					else window.location.hash = url;
				});
			});

			self.$searchWrap.on("click", self.playVideoBtn, function (e) {
				let watch = $(this).data("watch");

				if (watch.length > 0) e.preventDefault();
				else return; //open default link otherwise

				self.$playVideo.attr("src", "https://www.youtube.com/embed/" + watch + "?rel=0&showinfo=0&modestbranding=1&autoplay=1");
				self.$playVideoModal.modal("show");
			});

			self.$playVideoModal.on("hide.bs.modal", function () {
				self.$playVideo.attr("src", "");
			});
		},
		syncFilmsPage: function () {
			$(".filters .dropdown-menu").on("click.bs.dropdown", function (e) {
				e.stopPropagation();
				e.preventDefault();
			});

			// rating
			const ratingSlider = document.getElementById("filter-rating-slider");			
			const dataStartRating = $(ratingSlider).data("start");
			const startRating = Array.isArray(dataStartRating) ? dataStartRating : [0, 10];
			// eslint-disable-next-line no-undef
			noUiSlider.create(ratingSlider, {
				start: startRating,
				step: 0.1,
				connect: true,
				range: {
					"min": 0,
					"max": 10
				},
				format: {
					to: function (value) {
						return value;
					},
					from: function (value) {
						return value;
					}
				}
			});
			ratingSlider.noUiSlider.on("update", function(values, handle) {
				const labels = $("#filter-rating .filter-btn .filter-range");
				const start = labels.find("#filter-range-start");
				const end = labels.find("#filter-range-end");

				if(!handle) {
					start.html(values[0]);
					$("input[name='rating>']").val(values[0]);
				}
				else {
					end.html(values[1]);
					$("input[name='rating<']").val(values[1]);
				}
			});
			// genre
			$("#filter-genres-select").on("change", function(e){
				$("#filter-range-value").html($(this).val());
			});
			// year
			const yearSlider = document.getElementById("filter-year-slider");
			const dataStartYear = $(yearSlider).data("start");
			const startYear = Array.isArray(dataStartYear) ? dataStartYear : [1915, 2019];
			// eslint-disable-next-line no-undef
			noUiSlider.create(yearSlider, {
				start: startYear,
				step: 1,
				connect: true,
				range: {
					"min": 1915,
					"max": 2019
				},
				format: {
					to: function (value) {
						return value;
					},
					from: function (value) {
						return value;
					}
				}
			});
			yearSlider.noUiSlider.on("update", function(values, handle) {
				const labels = $("#filter-year .filter-btn .filter-range");
				const start = labels.find("#filter-range-start");
				const end = labels.find("#filter-range-end");

				if(!handle) {
					start.html(values[0]);
					$("input[name='releaseDate>']").val(values[0]);
				}
				else {
					end.html(values[1]);
					$("input[name='releaseDate<']").val(values[1]);
				}
			});
		},
		dashBoardSetup: function(){
			// open tab on page load
			const url = document.location.toString();
			const tabs = ".page-sidebar .navbar-side .nav-link";
			if (url.match("#")) {
				const attr = "[href=\"#" + url.split("#")[1] + "\"]";
				$(tabs + attr).tab("show");
			}

			$(tabs).on("shown.bs.tab", function (e) {
				// show message on tab open
				const alert = $(".page-content .alert");
				if (alert) {
					const alertHash = alert.data("for");
					if (e.target.hash != alertHash) alert.hide();
					else alert.show();
				}
				// add tab open to browser history
				if (history.pushState) window.history.pushState(null, null, e.target.hash);
				else window.location.hash = e.target.hash;	
			});

			//unpack genres for submit like array
			$("#addfilm form, #editfilm form:not(#filmsdeleteform)").submit(function () {
				const form = $(this);

				const genre = form.find("input[name=\"genre\"]");
				const genreVal = genre.tagsinput("items");

				genreVal.forEach(genreName => {
					form.prepend(
						"<input type=\"hidden\" name=\"genre\" value=\"" + genreName + "\">"
					);
				});

				genre.remove();
			});

			$("#confirmDelete").on("show.bs.modal", function (event) {
				const button = $(event.relatedTarget);
				const form = button.data("form");

				const modal = $(this);
				modal.find(".btn-danger").attr("form", form);
			});
		},
		syncDashBoardSetup: function () {
			const self = this;

			$(".datepicker-here").datepicker({
				dateFormat: "yyyy-mm-dd"
			});

			//
			let editUserStoreData = [];
			$("#editusers #users-select").selectize({
				valueField: "username",
				labelField: "username",
				searchField: "username",
				options: [],
				create: false,
				onLoad: (data) => {
					if(data && Array.isArray(data)) {
						data.forEach(element => {
							editUserStoreData.push(element);
						});
					}
				},
				onItemAdd: (value, $item) => userUpdateField($item),
				render: {
					item: (item, escape) => {
						return "<div data-order='" + item.$order + "'>" + escape(item.username) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.username) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/users?username=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						xhrFields: {
							withCredentials: true
						},
						crossDomain: true,
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			});

			function userUpdateField($item){
				console.log($item);
				$("#editusers input[name='user']").val($item.data("value"));

				const index = $item.data("order");
				const data = editUserStoreData[index - 1];
				if(data) {
					$("#editusers form input#editfullname").val(data.fullname);
					
					$("#editusers form select#editrole").val(data.role);
					
					$("#editusers form .user-fields").fadeIn();
				}
				else $("#editusers form .user-fields").fadeOut();
			}

			// 
			let editActorStoreData = [];
			$("#editactor #editactor-actors-select").selectize({
				valueField: "fullname",
				labelField: "fullname",
				searchField: "fullname",
				options: [],
				create: false,
				onLoad: (data) => {
					if(data && Array.isArray(data)) {
						data.forEach(element => {
							editActorStoreData.push(element);
						});
					}
				},
				onItemAdd: (value, $item) => actorUpdateField($item),
				render: {
					item: (item, escape) => {
						return "<div data-order='" + item.$order + "' data-slug='"+ escape(item.slug) + "'>" + escape(item.fullname) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.fullname) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/actors?fullname=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			});

			function actorUpdateField($item){
				let value = [];
				value.push( $item.data("slug") );
				$("input[name='actor']").val(value);

				const index = $item.data("order");
				const data = editActorStoreData[index - 1];
				if(data) {
					$("#editactor form input#editfullname").val(data.fullname);
					$("#editactor form textarea#editshortbio").val(data.shortbio);
					$("#editactor form textarea#editfullbio").val(data.fullbio);

					$("#editactor form input#editborn").val( data.born ? data.born.split("T")[0] : null );

					if(data.films && Array.isArray(data.films)) {
						editactorSelectControl.clear();
						editactorSelectControl.clearOptions();

						data.films.forEach(film => {
							$.ajax({
								url: self.BASE_API + "/films/" + film,
								type: "GET",
								dataType: "json",
								error: () => {},
								success: (response) => {
									if(response.data){
										editactorSelectControl.addOption(response.data);
										editactorSelectControl.addItem(response.data.title);
									}
								}
							});
						});
					}
					
					$("#editactor form select#editgender").val(data.gender);
					$("#editactor form input#editbudget").val(data.budget);
					$("#editactor form input#editcountryActor").val(data.country);
					
					$("#editactor form .actor-fields").fadeIn();
				}
				else $("#editactor form .actor-fields").fadeOut();
			}

			const editactorSelectControl = $("#editactor-films-select").selectize({
				valueField: "title",
				labelField: "title",
				searchField: "title",
				options: [],
				create: false,
				onChange: () => editActorUpdateFields(),
				render: {
					item: (item, escape) => {
						if(item.title && (item.slug == "undefined" || !item.slug)){
							item.slug = item.title.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
						}

						return "<div data-slug='"+ escape(item.slug) + "'>" + escape(item.title) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.title) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/films?title=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			})[0].selectize;

			editActorUpdateFields();
			function editActorUpdateFields(){
				let value = [];
				const items = editactorSelectControl.getValue();
				if(Array.isArray(items)) {
					items.forEach(item => {
						value.push( editactorSelectControl.getItem(item).data("slug") );
					});
				}
				else {
					value.push( editactorSelectControl.getItem(items).data("slug") );
				}

				$("#editactor input[name='films']").val(value);
			}

			const addactorSelectControl = $("#addactor-films-select").selectize({
				valueField: "title",
				labelField: "title",
				searchField: "title",
				options: [],
				create: false,
				onChange: () => addActorUpdateFields(),
				render: {
					item: (item, escape) => {
						if(item.title && (item.slug == "undefined" || !item.slug)){
							item.slug = item.title.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
						}

						return "<div data-slug='"+ escape(item.slug) + "'>" + escape(item.title) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.title) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/films?title=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			})[0].selectize;

			addActorUpdateFields();
			function addActorUpdateFields(){
				let value = [];
				const items = addactorSelectControl.getValue();
				if(Array.isArray(items)) {
					items.forEach(item => {
						value.push( addactorSelectControl.getItem(item).data("slug") );
					});
				}
				else {
					value.push( addactorSelectControl.getItem(items).data("slug") );
				}

				$("#addactor input[name='films']").val(value);
			}

			let editFilmStoreData = [];
			$("#editfilm #editfilms-select").selectize({
				valueField: "title",
				labelField: "title",
				searchField: "title",
				options: [],
				create: false,
				onLoad: (data) => {
					if(data && Array.isArray(data)) {
						data.forEach(element => {
							editFilmStoreData.push(element);
						});
					}
				},
				onItemAdd: (value, $item) => updateField($item),
				render: {
					item: (item, escape) => {
						return "<div data-order='" + item.$order + "' data-slug='"+ escape(item.slug) + "'>" + escape(item.title) + "</div>";
					},
					option: (item, escape) => {
						return "<div>" + escape(item.title) + "</div>";
					}
				},
				load: function(query, callback) {
					if (!query.length) return callback();
					$.ajax({
						url: self.BASE_API + "/films?title=/" + query + "/i",
						type: "GET",
						dataType: "json",
						data: {},
						error: function() {
							callback();
						},
						success: function(res) {
							callback(res.data);
						}
					});
				}
			});

			// updateField();
			function updateField($item){
				let value = [];
				value.push( $item.data("slug") );
				$("input[name='films']").val(value);

				const index = $item.data("order");
				const data = editFilmStoreData[index - 1];
				if(data) {
					$("#editfilm form input#edittitle").val(data.title);
					$("#editfilm form textarea#editshortdesc").val(data.shortdesc);
					$("#editfilm form textarea#editfulldesc").val(data.fulldesc);
					$("#editfilm form input#editrating").val(data.rating);

					$("#editfilm form input#editreleaseDate").val( data.releaseDate.split("T")[0] );

					if(data.genre && Array.isArray(data.genre)) {
						data.genre.forEach(genre => {
							$("#editfilm form  input#editgenre").tagsinput("add", genre.name);
						});
					}
					
					$("#editfilm form input#editytWatchID").val(data.ytWatchID);
					$("#editfilm form input#editbudget").val(data.budget);
					$("#editfilm form input#editcountryFilm").val(data.country);
					$("#editfilm form input#editlanguage").val(data.language);
					$("#editfilm form input#editproduction").val(data.production);
					
					$("#editfilm form .movie-fields").fadeIn();
				}
				else $("#editfilm form .movie-fields").fadeOut();
			}
		},
		signInAjax: function () {
			const self = this;

			self.$signinform.submit(function (e) {
				e.preventDefault();

				const form = $(this);
				const wrap = form.find(".form-info");
				const bar = wrap.find(".progress .progress-bar");
				const info = wrap.find(".text");

				$.ajax({
					method: "POST",
					url: self.BASE_API + "/auth/login?ajax=1",
					data: form.serialize(),
					beforeSend: () => {
						info.hide();
						bar.parent().show();
						form.css("padding-top", wrap.height() + "px");
						wrap.css("opacity", "1");
						bar.css("width", "13%");
					},
					complete: () => {
						setTimeout(() => {
							bar.css("width", "100%");

							info.fadeIn(() => {
								form.css("padding-top", wrap.height() + "px");
								bar.parent().fadeOut(() => {
									form.css("padding-top", wrap.height() + "px");
									bar.css("width", "0%");
								});
							});

							self.$signupform.find("input").each(function () {
								$(this).val("");
							});
						}, self.authLoaderTime);
					},
					error: function (response) {
						self.infoSetMessage(info, false, response.responseText);
					}
				}).done(function (response) {
					if (response.username) {
						self.infoSetMessage(info, true, "Welcome, " + response.username);
						setTimeout(() => {
							document.location.reload();
						}, self.authLoaderTime + self.authPageRedirectTime);
					} else if (response.message)
						self.infoSetMessage(info, false, response.message);
					else self.infoSetMessage(info, false, "Please, try later");
				});
			});
		},
		signUpValidation: function () {
			const self = this;

			self.$signupform.submit(function (e) {
				e.preventDefault();

				const form = $(this);

				form.find(".form-control").change();

				const warns = form.find(".has-warning");
				if (!warns.length) {
					const wrap = form.find(".form-info");
					const bar = form.find(".form-info .progress .progress-bar");
					const info = form.find(".form-info .text");

					$.ajax({
						method: "POST",
						url: self.BASE_API + "/auth/register?ajax=1",
						data: form.serialize(),
						beforeSend: () => {
							info.hide();
							bar.parent().show();
							form.css("padding-top", wrap.height() + "px");
							wrap.css("opacity", "1");
							bar.css("width", "13%");
						},
						complete: () => {
							setTimeout(() => {
								bar.css("width", "100%");

								info.fadeIn(() => {
									form.css("padding-top", wrap.height() + "px");
									bar.parent().fadeOut(() => {
										form.css("padding-top", wrap.height() + "px");
										bar.css("width", "0%");
									});
								});

								self.$signupform.find("input").each(function () {
									$(this).val("");
								});
							}, self.authLoaderTime);
						},
						error: function (response) {
							self.infoSetMessage(info, false, response.responseText);
						}
					}).done(function (response) {
						if (response.username)
							self.infoSetMessage(
								info,
								true,
								response.username + ", now you can sign in"
							);
						else self.infoSetMessage(info, false, "Please, try later");
					});
				}
			});

			self.$signupform.find("input").each(function () {
				$(this).change(function () {
					const input = $(this);
					if (!input.val().length) return self.inputToggleWarn(input);

					switch (input.attr("name")) {
					case "username":
						$.ajax({
							method: "GET",
							url: self.BASE_API + "/username/" + input.val()
						}).done(function (response) {
							if (response.user == true)
								self.inputToggleWarn(input, "username already taken");
							else self.inputToggleWarn(input);
						});

						break;
					case "password":
						if (input.val().length < 4 || input.val().length > 16)
							self.inputToggleWarn(
								input,
								"password length must be in range 4 - 16"
							);
						else self.inputToggleWarn(input);

						break;
					case "password_confirm": {
						const input2 = self.$signupform.find("[name=\"password\"]");

						if (input.val() != input2.val())
							self.inputToggleWarn(input, "passwords does not match");
						else self.inputToggleWarn(input);

						break;
					}
					}
				});
			});
		},

		infoSetMessage: function (info, success, message) {
			if (success)
				info
					.removeClass("bg-danger")
					.addClass("bg-success")
					.html(message);
			else
				info
					.removeClass("bg-success")
					.addClass("bg-danger")
					.html(message);
		},
		inputToggleWarn: function (input, message) {
			if (message) {
				input
					.addClass("has-warning")
					.parent()
					.find(".info")
					.html(message);
			} else {
				input
					.removeClass("has-warning")
					.parent()
					.find(".info")
					.html("");
			}
		}
	};

	$.loadScript = function (url, type = "script") {
		const options = {
			url: url,
			dataType: type,
			cache: true
		};
		return $.ajax(options);
	};

	$(document).ready(function () {
		new AwecinClass();

	});
})(jQuery);
