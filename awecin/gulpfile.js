const gulp = require("gulp");
const nodemon = require("gulp-nodemon");
const browserSync = require("browser-sync");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");

gulp.task("default", ["nodemon", "sass:watch", "browser-sync"], function() {});

gulp.task("browser-sync", function() {
  browserSync.init(null, {
    proxy: "http://localhost:3000",
    files: [
      "public/**/*.*",
      "public/*.*",
      "public/**/**/*.*",
      "views/*.*"
    ],
    port: 3001,
    notify: false
  });
});

gulp.task("nodemon", function(done) {
  let stream = nodemon({
    script: "./bin/www",
    ext: "js",
    watch: [
      "views/partials/*.*", // restart server on partials(!) changed
      "*.js",
      "./**/*.js"
    ],
    env: {
      NODE_ENV: "development",
      DEBUG: "awecin:*",
      DB_URL: "mongodb://root:pGaZa65{Z~@ds223063.mlab.com:23063/awecin-test1",
      CLOUD_NAME: "awecin",
      API_KEY: "775833174654551",
      API_SECRET: "QO-bwstTgl-UxIyhDW8G69GE3Wc",
      BASE_API_URL: "http://localhost:3000/api/v1/",
      SERVER_API_USER: "dI6pth6W1G5ikDAdICwR",
      SERVER_API_PASS: "tEWBZagYMeQacaTgxF0y"
    },
    done: done
  });

  stream.on("restart", function() {
    browserSync.reload();
  });
});

gulp.task("sass", function() {
  return gulp
    .src("./public/stylesheets/_sass/*.scss")
    .pipe(concat("style.scss"))
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("./public/stylesheets/"));
});

gulp.task("sass:watch", function() {
  gulp.watch("./public/stylesheets/_sass/*.scss", ["sass"]);
});
