const hbs = require("hbs");
const pagination = require("./pagination");

class Partials {
	static getAllowedKeysFilter() {
		return [ //allowed keys for search
			"page",
			"genre",
			"genre.slug", //
			"rating>",
			"rating<",
			"releaseDate>",
			"releaseDate<",
			"title"
		];
	}

	static filmsBlock(films, page, count, limit, query) {
		const allowedKeys = this.getAllowedKeysFilter();

		let queryFilters = "";
		for (let key in query) {
			if (!allowedKeys.includes(key)) continue;

			if (key == "genre.slug") { // convert from queryForAPI to valid href
				key = "genre";
				query[key] = query["genre.slug"].toString().toLowerCase();
				delete query["genre.slug"];
			}
			if (key == "title") { // convert from queryForAPI to valid href
				query[key] = query[key].replace("/i", "").replace("/", "");
			}
			if (key == "releaseDate>" || key == "releaseDate<") {
				const date = new Date(query[key]);
				query[key] = date.getFullYear();
			}
			queryFilters += "&" + key + "=" + query[key] + "";
		}

		const message = (query.title && query.title.length > 0) ? ("Search by title - \"" + query.title + "\"") : undefined;

		const paginationHtml = pagination(page, count, limit, "/films", queryFilters).render();
		const template = hbs.compile(hbs.handlebars.partials["films_block"]);
		const html = template({ films: films, pagination: paginationHtml, message });
		return new hbs.SafeString(html);
	}

	static filtersBlock(genres, query) {

		let currentGenre = "";
		if (query["genre.slug"]) {
			genres.forEach(genre => {
				if (genre.slug == query["genre.slug"]) {
					genre.active = true;
					currentGenre = genre.name;
				}
			});
		}
		const currentRatingGT = query["rating>"] ? query["rating>"] : undefined;
		const currentRatingST = query["rating<"] ? query["rating<"] : undefined;

		const releaseDateGT = query["releaseDate>"] ? new Date(query["releaseDate>"]).getFullYear() : undefined;
		const releaseDateST = query["releaseDate<"] ? new Date(query["releaseDate<"]).getFullYear() : undefined;

		const template = hbs.compile(hbs.handlebars.partials["filters_film"]);
		const html = template({
			genres,
			currentGenre,
			currentRatingGT,
			currentRatingST,
			releaseDateGT,
			releaseDateST
		});
		return new hbs.SafeString(html);
	}
}

module.exports = Partials;