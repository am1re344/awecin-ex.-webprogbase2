const hbs = require("hbs");
const pagination = require("./pagination");

class Partials {
	static getAllowedKeysFilter() {
		return [ //allowed keys for search
			"page",
			"born>",
			"born<",
			"fullname",
			"films"
		];
	}

	static actorsBlock(actors, page, count, limit, query) {
		const allowedKeys = this.getAllowedKeysFilter();

		let queryFilters = "";
		for (let key in query) {
			if (!allowedKeys.includes(key)) continue;

			if (key == "fullname") { // convert from queryForAPI to valid href
				query[key] = query[key].replace("/i", "").replace("/", "");
			}
			if (key == "born>" || key == "born<") {
				const date = new Date(query[key]);
				query[key] = date.getFullYear();
			}
			queryFilters += "&" + key + "=" + query[key] + "";
		}

		const paginationHtml = pagination(page, count, limit, "/actors", queryFilters).render();
		const template = hbs.compile(hbs.handlebars.partials["actors_block"]);
		const html = template({ actors, pagination: paginationHtml});
		return new hbs.SafeString(html);
	}

	static filtersBlock(query) {
		const searchName = query["fullname"] ? query["fullname"].replace("/i", "").replace("/", "") : undefined;
		const searchFilm = query.films ? query.films : undefined;

		const bornGT = query["born>"] ? new Date(query["born>"]).getFullYear() : undefined;
		const bornST = query["born<"] ? new Date(query["born<"]).getFullYear() : undefined;

		const template = hbs.compile(hbs.handlebars.partials["filters_actor"]);
		const html = template({
			searchName,
			bornGT,
			bornST,
			searchFilm
		});
		return new hbs.SafeString(html);
	}
}

module.exports = Partials;