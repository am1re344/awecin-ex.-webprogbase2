const createError = require("http-errors");

class Helper {
	static isLoggedIn(req, res, next) {
		let error = null;

		if (!req.user) error = createError(401);

		next(error);
	}

	static isAdmin(req, res, next) {
		let error = null;

		if (!req.user) error = createError(401);
		else if (req.user.role != 0) error = createError(403);

		next(error);
	}
}

module.exports = Helper;
