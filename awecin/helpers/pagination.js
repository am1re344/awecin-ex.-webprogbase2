const pagination = require("pagination");

var translations = {
	"PREVIOUS" : "<i class=\"fas fa-angle-left\"></i>",
	"NEXT" : "<i class=\"fas fa-angle-right\"></i>",
	// 'FIRST' : 'Eerst',
	// 'LAST' : 'Laatste',
	// 'CURRENT_PAGE_REPORT' : 'Resulten {FromResult} - {ToResult} van {TotalResult}'
};

// hello php

function paginate(currentPage, total, limit, prelink = "/", query = "") {
	return (new pagination.TemplatePaginator({
		current: currentPage,
		totalResult: total,
		prelink: prelink,
		rowsPerPage: limit,
		slashSeparator: false,
		translator : function(str) {
			return translations[str];
		},
		template: function(result) {
			let i, len, prelink;
			let html = "<nav class=\"mx-auto mt-5\"><ul class=\"pagination justify-content-center\">";

			if (result.pageCount < 2) {
				html += "</ul></div>";
				return html;
			}
			prelink = this.preparePreLink(result.prelink);
			if (result.previous) {
				html +=
          "<li class=\"page-item\"><a class=\"page-link\" href=\"" +
          prelink +
          result.previous +
          query +
          "\">" +
          this.options.translator("PREVIOUS") +
          "</a></li>";
			}
			if (result.range.length) {
				for (i = 0, len = result.range.length; i < len; i++) {
					if (result.range[i] === result.current) {
						html +=
              "<li class=\"active page-item\"><a class=\"page-link\" href=\"" +
              prelink +
              result.range[i] +
              query +
              "\">" +
              result.range[i] +
              "</a></li>";
					} else {
						html +=
              "<li class=\"page-item\"><a class=\"page-link\" href=\"" +
              prelink +
              result.range[i] +
              query +
              "\">" +
              result.range[i] +
              "</a></li>";
					}
				}
			}
			if (result.next) {
				html +=
          "<li class=\"page-item\"><a class=\"page-link\" href=\"" +
          prelink +
          result.next +
          query +
          "\" class=\"paginator-next\">" +
          this.options.translator("NEXT") +
          "</a></li>";
			}
			html += "</ul></nav>";
			return html;
		}
	}));
}

module.exports = paginate;
