const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const mongoosePaginate = require("mongoose-paginate");
const crypto = require("crypto");

const Schema = mongoose.Schema;

let UserSchema = new Schema({
	username: { type: String, lowercase: true, unique: true, required: true, match: [/^[a-zA-Z0-9]+$/, "is invalid"], minlength: 4, maxlength: 16, index: true },
	salt: {type: String, required: true},
	hash: {type: String, required: true},
	role: {type: Number, required: true, min: 0, max: 1},
	watchlist: [String],
	fullname: {type: String, minlength: 4, maxlength: 32},
	imgurl: String,
	// @todo email
	// email: match: [/\S+@\S+\.\S+/, 'is invalid']
    
}, { timestamps: true });

UserSchema.plugin(uniqueValidator, { message: "is already taken." });
UserSchema.plugin(mongoosePaginate);

UserSchema.methods.setPassword = function (password) {
	this.salt = crypto.randomBytes(16).toString("hex");
	this.hash = crypto.pbkdf2Sync(password, this.salt, 5000, 512, "sha512").toString("hex");
};

UserSchema.methods.validPassword = function (password) {
	let hash = crypto.pbkdf2Sync(password, this.salt, 5000, 512, "sha512").toString("hex");
	return this.hash === hash;
};

let model = mongoose.model("User", UserSchema);

module.exports = model;