const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Float = require("mongoose-float").loadType(mongoose);
const mongoosePaginate = require("mongoose-paginate");

const makeSlug = require("../helpers/makeslug");

const Schema = mongoose.Schema;

let FilmSchema = new Schema({
	title: { type: String, minlength: 3, maxlength: 32, required: true },
	slug: { type: String, required: true, index: true, unique: true },
	shortdesc: { type: String, required: true },
	genre: [
		{
			name: { type: String, minlength: 2, maxlength: 12 },
			slug: { type: String, minlength: 2, maxlength: 12, lowercase: true }
		}
	],
	rating: { type: Float, min: 0, max: 10 },
	fulldesc: String,
	imgurl: String,
	ytWatchID: { type: String, minlength: 8, maxlength: 15 }, // youtube trailer video id
	releaseDate: Date,
	budget: String,
	country: String,
	language: String,
	production: String
}, { timestamps: true }
);

FilmSchema.plugin(uniqueValidator, { message: "is already taken." });
FilmSchema.plugin(mongoosePaginate);

FilmSchema.methods.setSlug = function(title) {
	if (!title) return;
	this.slug = makeSlug(title);
};

let model = mongoose.model("Film", FilmSchema);

module.exports = model;
