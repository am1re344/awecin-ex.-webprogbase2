const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const mongoosePaginate = require("mongoose-paginate");

const makeSlug = require("../helpers/makeslug");

const Schema = mongoose.Schema;

let ActorSchema = new Schema({
	fullname: { type: String, minlength: 3, maxlength: 32, required: true },
	slug: { type: String, required: true, index: true, unique: true },
	shortbio: { type: String, required: true },
	fullbio: String,
	born: Date,
	country: String,
	gender: String,
	imgurl: String,
	films: [String],

}, { timestamps: true }
);

ActorSchema.plugin(uniqueValidator, { message: "is already taken." });
ActorSchema.plugin(mongoosePaginate);

ActorSchema.methods.setSlug = function(fullname) {
	if (!fullname) return;
	this.slug = makeSlug(fullname);
};

let model = mongoose.model("Actor", ActorSchema);

module.exports = model;
