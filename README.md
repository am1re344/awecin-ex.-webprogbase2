![Logo](media/logo.png)

About
----------------
Awecin it is online cinema where stored information about films, TV Series and actors.
Hopefully it will have pricing plans for watching premium films.
Developed on NodeJS with Express & MongoDB.

**Project status**: developing

[Specification](https://docs.google.com/document/d/1oFfqdBikqjyMUZFqZw8jV0By_IM4fsHAMqW0M2kNiKo)